
let runningReaction = null

const obj = {
    a: 0,
    b: 1
}

// Call fn when deps is changed
autoRun(() => {
    console.log("obj.a", obj.a)
})

autoRun(() => {
    console.log("obj.b", obj.b * 2)
})

function reactive(obj) {
    return Object.entries(obj).reduce((acc, [key, value]) => {
        let val = value;
        const deps = new Set();
        Object.defineProperty(acc, key, {
            get: () => {
                if(runningReaction && !deps.has(runningReaction)){
                    deps.add(runningReaction)
                }
                return val
            },
            set: (newValue) => {
                if (hasChanged(newValue, val)) {
                    val = newValue
                    deps.forEach(fn => fn())
                }
            },
            enumerable: true
        })
        return acc
    })
}

function hasChanged(oldVal, newVal) {
    return oldVal !== newVal && (oldVal == oldVal || newVal == newVal)
}

function autoRun(fn) {
    runningReaction = fn;
    fn();
    runningReaction = null;
}


